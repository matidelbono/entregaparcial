﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    class Principal
    {
        public List<Seguro> Seguros = new List<Seguro>();
        public List<Siniestro> Siniestros = new List<Siniestro>();
        public bool BuscarSeguroPorPoliza(int NumeroPoliza)
        {
            foreach (var seguro in Seguros)
            {
                if (NumeroPoliza == seguro.NumeroPoliza)
                    return true;
            }
            return false;
        }
        
}
}


