﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
   abstract class Seguro
    {
       public string NombreTomador {get; set;}
       public string ApellidoTomador { get; set; }
       public int DNITomador { get; set; }
       public string NombreBeneficiario { get; set; }
       public string ApellidoBeneficiario { get; set; }
       public int DNIBeneficiario { get; set; }
       public decimal PrimaAsegurada { get; set; }
       public int NumeroPoliza { get; set; }
       public decimal CostoDelProducto { get; set; }
       
        public decimal CalcularPorcentajeCobertura()
        {
            return (CostoDelProducto * 12) / PrimaAsegurada;

        }
        public abstract bool SePuedeAgregarSeguro(int DNIInformador);
        
        
}
}

