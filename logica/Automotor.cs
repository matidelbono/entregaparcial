﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    abstract class Automotor: Seguro
    {
        public int PatenteVehiculo { get; set; }
        public string Marca { get; set; }
        public int AnoDeFabricacion { get; set; }
        public int Ano = DateTime.Today.Year;
        public bool EsMoto { get; set; }
        public int CantidadOcupantes { get; set; }
        public int CalcularEdadVehiculo()
        {
            return Ano - AnoDeFabricacion;
        }
        public  override bool SePuedeAgregarSeguro(int DNIInformador)
        {
            if (DNIInformador == DNIBeneficiario && CalcularEdadVehiculo() <= 5)
                return true;
          else
                return false;
        }
        public override string ObtenerDescripcion()
        {
            string descripcion = "automotor";
            return descripcion;
        }
    }
}
