﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    class Siniestro
    {
        public DateTime FechaSiniestro { get; set; }
        public int DNIInformador { get; set; }
        public int NumeroPoliza { get; set; }
      

        public Siniestro(DateTime fechaSiniestro, int dNIINFORMADOR, int  numeroPoliza)
        {
            FechaSiniestro = fechaSiniestro;
            DNIInformador = dNIINFORMADOR;
            NumeroPoliza = numeroPoliza;
        }
    }
}

