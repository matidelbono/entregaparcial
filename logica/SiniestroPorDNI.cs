﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    class SiniestroPorDNI
    {
        public string NombrePorDNI { get; set; }
        public string ApellidoPorDni { get; set; }
        public string SseguroContratado { get; set; }
        public DateTime fechaSiniestro { get; set; }
       public int NumeroPoliza { get; set; }
    }
}
