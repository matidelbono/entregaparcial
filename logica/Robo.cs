﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    abstract class Robo: Seguro
    {
        public int ModeloDelTelefono { get; set; }
        public int TamanoPantalla { get; set; }
        public decimal PrecioActual { get; set; }
        public DateTime FechaDeCompra { get; set; }
        public override bool SePuedeAgregarSeguro(int DNIInformador)
        {
            if (DNIInformador == DNIBeneficiario && DNIInformador == DNITomador && TamanoPantalla < 6)
                return true;
            else
                return false;
        }
        public override string ObtenerDescripcion()
        {
            string descripcionRobo = "Robo";
            return descripcionRobo;
        }
    }
}
